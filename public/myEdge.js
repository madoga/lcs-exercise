class MyEdge{
    constructor(source, target, lcs){
        this.source = source;
        this.target = target;
        this.lcs = lcs;
    }
}