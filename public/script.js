var w = 500;
var h = 800;
var padding = 30;

var xScale;

var datasetNodes;
var datasetEdges;

var orderToId = [];

var sumLcs;
var currentlcs;
var markedNode;
var arrayNodes = [];
var arrayEdges = [];
d3.json("graph.json", function (data) {
    sumLcs = d3.sum(data.edges, function(d){return d.lcs});
    currentlcs = sumLcs/2;
    xScale = d3.scalePow()
        .domain([
            d3.min(data.nodes, function (d) {return dateValueOfString(d.date)}),
            d3.max(data.nodes, function (d) {return dateValueOfString(d.date)})
        ])
        .range([padding, w- padding]);

    yScale = d3.scaleLinear()
        .domain([
            0,
            sumLcs
        ])
        .range([padding, h- padding]);

    xPercentScale = d3.scaleLinear()
        .domain([0,100])
        .range([padding, w - padding]);

    var svg = d3.select("body")
        .append("svg")
        .attr("width", w)
        .attr("height", h);



    //generate dataset
    for(var i = 0; i < data.nodes.length; i++){
        var currNode = data.nodes[i];
        arrayNodes.push(new MyNode(currNode.id, dateValueOfString(currNode.date), currNode.sequence));
    }
    // set order of node
    var currentMinimumDate = d3.max(arrayNodes, function (d){return d.numericDate});
    var currentMinimumNode;

    for (var order = 0; order < arrayNodes.length; order++){
        for (var j = 0; j < arrayNodes. length; j++){
            if (currentMinimumDate > arrayNodes[j].numericDate && arrayNodes[j].order < 0){
               currentMinimumDate = arrayNodes[j].numericDate;
               currentMinimumNode = arrayNodes[j];
            }
        }

        if (order == 0){
            currentMinimumNode.marked = true;
            markedNode = currentMinimumNode;
        }
        currentMinimumNode.order = order;
        currentMinimumNode.xValue = xPercentScale((order*100)/ arrayNodes.length);
        orderToId.push(currentMinimumNode.id);
        currentMinimumDate = d3.max(arrayNodes, function (d){return d.numericDate}) + 1;

    }

   for (var i = 0; i < data.edges.length; i++){
       arrayEdges.push(new MyEdge(data.edges[i].nodes[0], data.edges[i].nodes[1],  data.edges[i].lcs));
   }
   arrayEdges.sort(function (a,b) { return a.source - b.source });

   var inverter = 1;
   arrayNodes[orderToId[0]].yValue = yScale(currentlcs);
    arrayNodes[orderToId[0]].currentlcs = currentlcs;
    for (var i = 0; i < data.nodes.length; i++){
      for (var j = 0; j < data.edges.length -1; j++){
          if ((arrayEdges[j].source == orderToId[i] && arrayEdges[j].target == orderToId[i+1]) ||
              arrayEdges[j].source == orderToId[i+1] && arrayEdges[j].target == orderToId[i]){
               var currentcurrentlcs;
              currentcurrentlcs = currentlcs - (5 * sumLcs / (inverter *  arrayEdges[j].lcs));
              inverter = -1 * inverter;
              arrayNodes[orderToId[i+1]].currentlcs = currentcurrentlcs;
              arrayNodes[orderToId[i+1]].yValue = yScale(currentcurrentlcs);
          }
      }
   }
   svg.selectAll("line")
       .data(arrayEdges)
       .enter()
       .append("line")
       .attr("x1", function (d) {return arrayNodes[d.source].xValue;})
       .attr("y1", function (d) {return arrayNodes[d.source].yValue;})
       .attr("x2", function (d) {return arrayNodes[d.target].xValue;})
       .attr("y2", function (d) {return arrayNodes[d.target].yValue;})
       .style("stroke", function (d) {
          if (arrayNodes[d.source].marked || arrayNodes[d.target].marked){
              return "orange";
          }else{
              return "lightgrey";
          }
       });


   var tooltip = d3.select("body")
           .append("div")
           .style("position", "absolute")
           .style("z-index", "10")
           .style("visibility", "hidden")
           .text("");

   svg.selectAll("circle")
       .data(arrayNodes)
       .enter()
       .append("circle")
       .attr("cx", function (d) {
           return d.xValue;
       })
       .attr("cy", function (d) {return d.yValue;})
       .attr("r", 10)
       .attr("fill", function (d) {
           if(markedNode.order > d.order){
               return "lightgrey";
           }
           if (d.marked){
               return "orange";
           }else{
               return "blue";
           }
       })
       .on("mouseover", function(d){
           arrayNodes[orderToId[0]].marked = false;
           d.marked = true;
           markedNode = d;
           updateValues(arrayNodes, arrayEdges, orderToId, markedNode);
           updateNodes(svg, arrayNodes, orderToId);
           updateEdges(svg, arrayEdges, arrayNodes);
           updateTooltip(tooltip, markedNode);
       })
       .on("mouseout", function(d){
           d.marked = false;
           arrayNodes[orderToId[0]].marked = true;
           markedNode = arrayNodes[orderToId[0]];
           updateValues(arrayNodes, arrayEdges, orderToId, markedNode);
           updateNodes(svg, arrayNodes, orderToId);
           updateEdges(svg, arrayEdges, arrayNodes);
           updateTooltip(tooltip, markedNode);
       });
    updateTooltip(tooltip, markedNode);
});

function updateValues(arrayNodes, arrayEdges, orderToId, markedNode) {
    var currentlcs = markedNode.currentlcs;
    markedNode.yValue = yScale(currentlcs);
    var inverter = -1;
    for (var i = markedNode.order; i < arrayNodes.length; i++){
        for (var j = 0; j < arrayEdges.length -1; j++){
            if ((arrayEdges[j].source == orderToId[i] && arrayEdges[j].target == orderToId[i+1]) ||
                arrayEdges[j].source == orderToId[i+1] && arrayEdges[j].target == orderToId[i]){
                var currentcurrentlcs;
                currentcurrentlcs = currentlcs - (5 * sumLcs / (inverter *  arrayEdges[j].lcs));
                inverter = -1 * inverter;
                arrayNodes[orderToId[i+1]].currentlcs = currentcurrentlcs;
                arrayNodes[orderToId[i+1]].yValue = yScale(currentcurrentlcs);
            }
        }
    }
}


function updateTooltip(tooltip, markedNode) {
   tooltip.text(markedNode.string);
   tooltip.style("visibility", "visible");
}


function updateNodes(svg, arrayNodes, oderToId){
    svg.selectAll("circle")
        .data(arrayNodes)
        .attr("cx", function (d) {
            return d.xValue;
        })
        .attr("cy", function (d) {return d.yValue;})
        .attr("r", 10)
        .attr("fill", function (d) {
            if(markedNode.order > d.order){
                return "lightgrey";
            }
            if (d.marked){
                return "orange";
            }else{
                return "blue";
            }
        });


}
function updateEdges(svg, arrayEdges, arrayNodes){
    svg.selectAll("line")
        .data(arrayEdges)
        .attr("x1", function (d) {return arrayNodes[d.source].xValue;})
        .attr("y1", function (d) {return arrayNodes[d.source].yValue;})
        .attr("x2", function (d) {return arrayNodes[d.target].xValue;})
        .attr("y2", function (d) {return arrayNodes[d.target].yValue;})
        .style("stroke", function (d) {
            if ((arrayNodes[d.source].marked || arrayNodes[d.target].marked)
                && (arrayNodes[d.source].order > markedNode.order || arrayNodes[d.target].order > markedNode.order)){
                return "orange";
            }else{
                return "lightgrey";
            }
        });

}



//functions
function dateValueOfString (string) {
   var split = string.split("-");
   var mydate = new Date(split[0], split[1] - 1, split[2]);
   return mydate.valueOf();
}

